# [ 活動 ] Taiwan VMware User Group (VMUG) 5 月份活動
## 活動內容
- 題目：**vSphere Web Client 的另一種選擇：GOVC**
- 時間：5/20 ( 六 ) 21:00 ~ 22:00
## 分享資訊
提供以下內容：
- 簡報（PDF）[[線上瀏覽]](https://gitlab.com/farmer871/learning_govc/-/blob/main/vSphere%20Web%20Client%20%E7%9A%84%E5%8F%A6%E4%B8%80%E7%A8%AE%E9%81%B8%E6%93%87_%20GOVC.pdf),[[下載]](https://gitlab.com/farmer871/learning_govc/-/raw/main/vSphere%20Web%20Client%20%E7%9A%84%E5%8F%A6%E4%B8%80%E7%A8%AE%E9%81%B8%E6%93%87_%20GOVC.pdf?inline=false)
- 實驗指南 [[連結]](https://hackmd.io/@farmer87/govc_labguide) << 已更新！
  > 另外會同時公開於 [Hackmd.io](https://hackmd.io/)
---
#### 感謝當天活動主持人及參與者
